## Terraform reusable module project

- Custom module for AWS VPC and EC2.
- Used AWS S3 for tf backend, i.e. to save state.
- Used Custom runner/ EC2 instance for running pipeline, and installed aws cli, Terraform.

### Prerequisite
- Create a IAM role for resources created by terraform
- Create ssh keys for EC2, using ssh-keygen
- Configure Gitlab-runner user with aws cli
- Use S3 versioning for history of state of TF
